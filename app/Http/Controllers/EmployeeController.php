<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\WorkPlace;

use App\Models\Entity;

use Illuminate\Support\Facades\DB;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Entity::all();
        return view('employee.index',compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workplace = WorkPlace::all();
        return view('employee.conf_employee',compact('workplace'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $employee= new Entity();
        $data = request()->all();
        $employee->fill($data);
     
        $employee->save();
        return redirect()->route('employee')->with('success','Registro Creado Exitosamente!!');
                        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee= Entity::find($id);
        //return response()->json([$employee], 200);
        return view('employee.show',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*request()->validate([
            'name' => 'required',
            'email' => 'required',
        ]);*/
        $workplace = WorkPlace::all();
        $employee= Entity::find($id);
        //return response()->json([$employee], 200);
        return view('employee.edit',compact('employee','workplace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee= Entity::find($id);
       
        return response()->json([$employee], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee= Entity::find($id);
        $employee->delete();
        return redirect()->route('employee')->with('success','Registro borrado !!');
     
    }
}
