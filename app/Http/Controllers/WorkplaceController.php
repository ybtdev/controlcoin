<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\WorkPlace;

use App\Models\Entity;

use Illuminate\Support\Facades\DB;


class WorkplaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workplace = WorkPlace::all();
        return view('workplace.index',compact('workplace'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workplace.create',compact('workplace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $workPlace= new WorkPlace();
        $data = request()->all();
        $workPlace->fill($data);
     
        $workPlace->save();
        
        return redirect()->route('workplace')->with('success','Registro Creado Exitosamente!!');
                        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workplace= WorkPlace::find($id);
        return response()->json([$workplace], 200);
        //return view('workplace.show',compact('workplace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workplace= WorkPlace::find($id);
        return response()->json([$workplace], 200);
              //return view('employee.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workplace= WorkPlace::find($id);
        return response()->json([$workplace], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workplace= WorkPlace::find($id);
        $workplace->delete();
        return redirect()->route('workplace')->with('success','El cargo ha sido Borrado');

    }
}
