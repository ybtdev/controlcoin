<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class BTController extends Controller
{
    //Real time
    public function realTime(){
        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/realtime/hashrate',
            [
            'query' => [
                'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                'puid'=> Auth::user()->puid                ]
            ]
        );
  
         $response = json_decode($res->getBody()->getContents());

         return response()->json(
             ['msg'=>'hashrate',
              'datos'=>$response->data,
             'code'=>200
             ],
            200);
    }

    //Mineros  Real-time stats
    public function mineros(Request $request){
        $client = new \GuzzleHttp\Client();
        
        $res = $client->request('GET', 'http://us-pool.api.btc.com/v1/worker/stats',
            [
            'query' => [
                'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                'puid'=>  Auth::user()->puid,
                ]
            ]
        );
  
        $response = json_decode($res->getBody()->getContents());

         return response()->json(
             [
              'msg'=>'mineros',
              'datos'=>$response->data,
              'code'=>200
             ],
            200);
    }

    //Ganancias GET /account/earn-stats
    public function gananciasHoy(Request $request){
        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/account/earn-stats',
            [
            'query' => [
                'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                'puid'=> Auth::user()->puid,
                'page'=>'1',
                'page_size'=>'2'
                ]
            ]
        );
  
        $response = json_decode($res->getBody()->getContents());
        //"total_paid": 5356111,   total_pagado
        //"earnings_today": 188552,     estimado hoy
        //"unpaid": 188552--Balance
        //"earnings_yesterday" : 204322   ayer
        //"pending_payouts" : 0
        //"last_payment_time" : 14111 microtime

        return response()->json(
             ['msg'=>'ganancias',
              'datos'=>$response->data,
             'code'=>200
             ],
            200);
    }

    //Poll GET /account/earn-stats
    public function poolHasrate(){

            $client = new \GuzzleHttp\Client();

          /*  https://us-pool.api.btc.com/v1/worker/share-history?
            dimension=1h
            start_ts=1519842217
            real_point=1
            count=72
            access_key=HlQW51bpJz6SlKnSg854GA4xc3ZYSUrMOXHpUBB0
            puid=170371&lang=en*/

            $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/pool/multi-coin-stats',
                [
                'query' => [
                    'dimension' => '1h'
                    ]
                ]
            );
      
            $response = json_decode($res->getBody()->getContents());


        return response()->json(
             ['msg'=>'poolHasrate',
              'datos'=> $response->data,
             'code'=>200
             ],
            200);
    }
     //Poll GET /account/earn-stats
    public function usuario(){

            $client = new \GuzzleHttp\Client();

           $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/account/info',
                [
                'query' => [
                    'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                    'puid'=> Auth::user()->puid,
                    ]
                ]
            );
      
            $response = json_decode($res->getBody()->getContents());


        return response()->json(
             ['msg'=>'usuario',
              'datos'=> $response->data,
              'code'=>200
             ],
            200);
    }


    //Network status

    public function networkStatus(){

            $client = new \GuzzleHttp\Client();

            $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/coins-income',
                [
                'query' => [
                    'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                    'puid'=>  Auth::user()->puid,
                    ]
                ]
            );
      
            $response = json_decode($res->getBody()->getContents());


        return response()->json(
             ['msg'=>'usuario',
              'datos'=> $response->data,
              'code'=>200
             ],
            200);
    }

    public function grafica(){

        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/worker/share-history',
            [
                'query' => [
                    'dimension' => '1h',
                    'start_ts' => '1519689846',
                    'count' => '24',
                    'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                    'puid'=> Auth::user()->puid,
                ]
            ]
        );


        $response = json_decode($res->getBody()->getContents());

        $categorias = array();
        $valores = array();

        foreach ($response->data->tickers as $key => $value) {

            foreach ($value as $k => $v) {
                if($k == 0){
                    $categorias[] =date('H:i', $v);
                }
                if($k == 1){
                    $valores[] = $v;
                }
            }
        }

        return response()->json(
            ['msg'=>'grafica',
                'datos'=> array("categorias"=>$categorias, "valores"=>$valores),
                'code'=>200
            ],
            200);
    }



    public function datosMineros(){

        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $res = $client->request('GET', 'https://us-pool.api.btc.com/v1/worker/',
            [
                'query' => [
                    'group' => '-1',
                    'page' ==   '1',
                    'page_size' => '50',
                    'status' => 'all',
                    'order_by' => 'worker_name',
                    'asc' => '1',
                    //'start_ts' => '1519689846',
                    //'count' => '100',
                    'access_key'=>'TwQFZdNIsFSwD4DUPe7aDmXo77MZ3zhJze1rjTIG',
                    'puid'=> Auth::user()->puid,
                ]
            ]
        );
  
        $response = json_decode($res->getBody()->getContents());
        $data =  array();
        
        foreach ($response->data->data as $key => $mineros) {
            $arr = array();

            foreach ($mineros as $k => $v) {
                if($k == "worker_name")
                    $arr["equipo"] = $v;

                if($k == "shares_1m")
                    $arr["tiempo_real"] = $v;

                if($k == "shares_1d")
                    $arr["diario"] = $v;

                if($k == "accept_count")
                    $arr["aceptado"] = $v;

                if($k == "reject_percent")
                    $arr["rechazado"] = $v;

                if($k == "last_share_time")
                    $arr["ultima_conexion"] = date('Y-m-d H:i AM', $v);

                if($k == "status")
                    $arr["estatus"] = $v;
            }

            $data["data"][] = $arr;
        }


    return response()->json(
            ['msg'=>'datos_minero',
                'datos'=> $data,
                'code'=>200
            ],
            200);
    //return view('layouts.mineros.index',compact('data'));

    }
}
