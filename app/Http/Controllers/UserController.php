<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Cookie;

class UserController extends Controller
{

    /**
     * @var object
     */
    private $client;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function add(Request $request)
    {

    }

    public function showAdmin()
    {

        return redirect()->route('home');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->username,
            'password' => $request->password
        ])) {

            return redirect('/');

        } else {

            return redirect('login')->with('error', 'PROBLEMAS!!');
        }

    }

    public function logOut()
    {
        Auth::logout();
        Session::flush();
        Cookie::forget('laravel_session');
        return redirect('login')->with('mensaje_error', 'Tu sesion ha sido cerrada.');
    }

    public function showLogin()
    {
        // Verificamos que el usuario no esta autenticado
        if (Auth::check()) {
            // Si esta autenticado lo mandamos a la rai­z donde estara el mensaje de bienvenida.
            return redirect()->route('home');
        }
        // Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)

        return view('login');
    }


}
