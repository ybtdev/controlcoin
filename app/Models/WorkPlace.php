<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{
    protected $table = 'workplace';
    protected $primaryKey = 'work_id';
    
    public $timestamps = false;
    
    protected $fillable = ['work_name', 'work_alias', 'work_estatus'];
}
