<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            

        DB::table('users')->delete();
        $users = array(
            ['name' => 'misael', 'puid' => '170371', 'email' => 'info@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'camenuser',   'puid' => '175562','email' => 'info1@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'reynaldo01',  'puid' => '142926','email' => 'info2@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'ronald01',    'puid' => '142927','email' => 'info3@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'Giorgio',    'puid' => '144860','email' => 'info4@gmail.com', 'password' => Hash::make('123456')],

            ['name' =>'sandrazuloaga',    'puid' => '150287','email' => 'info6@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'luisroque',    'puid' => '150378','email' => 'info7@gmail.com', 'password' => Hash::make('123456')],

            ['name' =>'yobet2',       'puid' => '151267','email' => 'info8@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'albatros',     'puid' => '152399','email' => 'info10@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'eduardouser',  'puid' => '152683','email' => 'info11@gmail.com', 'password' => Hash::make('123456')],

            ['name' =>'titouser',  'puid' => '154844','email' => 'info12@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'leoneluser',  'puid' => '157004','email' => 'info13@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'salaminavzla',  'puid' => '166603','email' => 'info14@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'walteruser2',  'puid' => '170245','email' => 'info15@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'gregonro',    'puid' => '170297','email' => 'info16@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'josephuser',  'puid' => '171132','email' => 'info18@gmail.com', 'password' => Hash::make('123456')],

            ['name' =>'yrisuser',  'puid' => '175025','email' => 'info19@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'hidalgouser',  'puid' => '175046','email' => 'info20@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'jairouser',  'puid' => '175050','email' => 'info21@gmail.com', 'password' => Hash::make('123456')],
            ['name' =>'fadiuser',  'puid' => '175509','email' => 'info22@gmail.com', 'password' => Hash::make('123456')],


           
        );
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user) {
            User::create($user);
        }
        
    }
}
