@extends('layouts/default')

{{-- Page title --}}
@section('title')
  Bienvenido a la plataforma
  @parent
@endsection

@section('content')
  <div class="row">
      <div class="col-lg-2">
          <!-- Column -->
            <div class="card">
                <div class="card-body text-left">
                  <div class="d-flex row">
                    <div class="col-md-12">
                      <h2 class="card-title font-16">Mineros: <span id="workers_total">0</span></h2>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 p-t-10">
                      <h2 class="font-light  font-16">Activos :  <span id="workers_activos">0</span></h2>
                    </div>

                    <div class="col-12 p-t-10">
                      <h2 class="font-light font-16">Inactivos : <span id="workers_inactivos">0</span></h2>
                    </div>

                  </div>
                </div>
          </div>
          <!-- Column -->

         <div class="card">
            <div class="card-body">
                <h4 class="card-title">Total GH/s</h4>
                <div class="d-flex">
                    <div class="align-self-center">
                        <h4 class="font-medium m-b-0" id="_total"><i class="ti-angle-up text-success"></i>  </h4></div>
                    <div class="ml-auto">
                        <div id="spark8"><canvas width="67" height="40" style="display: inline-block; width: 67px; height: 40px; vertical-align: top;"></canvas></div>
                    </div>
                </div>
            </div>
        </div>

      </div>

      <!-- Column -->
      <div class="col-lg-10">
              <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Minadores del usuario</h4>
                                <hr>
                                <div class="table-responsive m-t-20">
                                    <table id="_mineria" class="display nowrap table table-hover table-striped table-bordered color-bordered-table  info-bordered-table" cellspacing="0" width="100%">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <th>Equipo</th>
                                                <th class="text-right">Tiempo Real</th>
                                                <th class="text-right">Diario</th>
                                                <th>Aceptado</th>
                                                <th>Rechazado</th>
                                                <th>Ultimo </th>
                                                <th>Estatus </th>
                                            </tr>

                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th id="_real" class="text-right">Total</th>
                                                <th id="_diario" class="text-right">Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            
                                        </tfoot>
                                        <tbody id="_cuerpo_minero">
                                            <tr>
                                                <td>Equipo</td>
                                                <td class="text-right">Tiempo Real</td>
                                                <td  class="text-right">Diario</td>
                                                <td>Aceptado</td>
                                                <td>Rechazado</td>
                                                <td>Ultimo </td>
                                                <td>Estatus </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
      <!-- Column -->
      </div>
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

<script>

$(".text-themecolor").text('Mineros');
$("#_titulo").text('Mineros');
const url = $("meta[name=base_url]").attr('content');
var res;

      function datos_tabla(){
        _cad = ''; 
        $.ajax({
            async: false,
            url: url+'/datosMineros',
            type: 'get',
            dataType: "json",
            //data:{},
            success: function(data) {

              if(data.datos !=''){

                $.each(data.datos, function(i,v){
                  $.each(v, function(j,minero){
                  estatus ='';
                  _diario = 0;
                  _real = 0;

                    if(minero.estatus=='ACTIVE')
                      estatus = `<span class="label label-info">ACTIVO</span>`;
                    else
                      estatus = `<span class="label label-info">INACTIVO</span>`;

                      _cad +=`<tr><td>${minero.equipo}</td>
                              <td>${minero.tiempo_real}GH/s</td>
                              <td>${minero.diario}GH/s</td>
                              <td>${minero.aceptado}</td>
                              <td>${minero.rechazado}</td>
                              <td>${minero.ultima_conexion}</td>
                              <td>${estatus}</td></tr>`;
                      _diario =(_diario+minero.diario);
                      _real   =(_real+minero.tiempo_real);
                  });
                });
                $("#_cuerpo_minero").html(_cad);
                $("#_total").html(_diario+_real);
                $("#_diario").html(_diario+'GH/s');
                $("#_real").html(_real+'GH/s');
              }
            }
        });

     }


   datos_tabla();
      datos_tabla();

          $('#_mineria').DataTable( {
            "language": {
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primeo",
                        "last":       "Ultimo",
                        "next":       "Proximo",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": activar para ordenar la columna ascendente",
                        "sortDescending": ": activar para ordenar la columna descendente"
                    }
                }
        });

 
 //mineros
 function mineros(){
    $.ajax({
        url: url+'/mineros',
        type: 'get',
        dataType: "json",
        //data:{},
        success: function(data) {
            $("#workers_activos").html(data.datos.workers_active); //activos
            $("#workers_inactivos").html(data.datos.workers_inactive); //inactivos
            $("#workers_total").html(data.datos.workers_total); //total
        }
    });
 }          

mineros();

</script>
@endsection
