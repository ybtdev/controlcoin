@extends('layouts/default')

{{-- Page title --}}
@section('title')
   Bienvenido a la plataforma
   @parent
@endsection

@section('content')
   <div class="row">
      <div class="col-lg-3">
         <!-- Column -->
         <div class="card"><img class="card-img-top" src="{{ asset('assets/images/background/profile-bg.jpg') }}"
                                alt="Card image cap" style="max-height: 109px;">
            <div class="card-body little-profile text-center">
               <div class="pro-img"><img src="{{ asset('assets/images/users/3.png') }}" alt="user"/></div>
               <h3 class="m-b-0" id="_usuario">Markarn Doe</h3>
            </div>
         </div>
         <!-- Column -->

         <div class="card">
            <div class="card-body">
               <!-- Row -->
               <div class="row">
                  <div class="col-12">
                     <h2>Pool Hashrate </i></h2>
                     <h2 class="text-primary"><span id="_pool"></span> EH/s</h2>
                  </div>
                  <div class="col-12">
                     <div class="progress">
                        <div id="_bar_red" class="progress-bar bg-info" role="progressbar"
                             style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                             aria-valuemax="100"></div>
                     </div>
                  </div>
                  <div></div>
                  <div class="col-12" style="border-top: outset; border-width: 1px;  margin-top: 35px;">
                     <h2 style="margin-top: 20px;"><i>Pool de la red </i></h2>
                     <h2 class="text-primary"><span id="_pool_redes"></span> EH/s</h2>
                  </div>
                  <div class="col-12">
                     <div class="progress">
                        <div id="_bar_red" class="progress-bar bg-warning" role="progressbar"
                             style="width: 95%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                             aria-valuemax="100"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Column -->
      <div class="col-lg-9 ">
         <div class="row">
            <!--tiempo real -->
            <div class="col-lg-6">
               <div class="card card-inverse">
                  <div class="card-body text-center text-default">
                     <div class="d-flex row">
                        <div class="col-md-12 m-r-20">
                           <h1 class=""><i class="mdi mdi-elevation-rise"></i></h1>
                        </div>
                        <div class="col-md-12">
                           <h3 class="card-title  text-primary">BTC Hashrates</h3>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light  font-18">Tiempo Real</h2>
                           <h2 class="font-light " id="_real_time">$14,000</h2>
                        </div>
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light  font-18">24 H</h2>
                           <h2 class="font-light " id="_hashrate_hoy">$14,000</h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- mineros -->
            <div class="col-lg-6">
               <div class="card card-inverse card-info">
                  <div class="card-body text-center">
                     <div class="d-flex row">
                        <div class="col-md-12 m-r-20">
                           <h1 class="text-white"><i class="fa fa-gears"></i></h1>
                        </div>
                        <div class="col-md-12">
                           <h3 class="card-title">Mineros: <span id="workers_t">0</span></h3>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light text-white font-18">Activos</h2>
                           <h2 class="font-light text-white" id="workers_a">0</h2>
                        </div>
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light text-white font-18">Inactivos</h2>
                           <h2 class="font-light text-danger" id="workers_i">0</h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-lg-6">
               <div class="card card-inverse card-info">
                  <div class="card-body text-center">
                     <div class="d-flex row">
                        <div class="col-md-12 m-r-20">
                           <h1 class="text-white"><i class="fa fa-bitcoin"></i></h1>
                        </div>
                        <div class="col-md-12">
                           <h3 class="card-title">BTC Earnings</h3>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light text-white font-18">Estimado de Hoy</h2>

                        </div>
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h2 class="font-light text-white font-18">Ayer</h2>

                        </div>
                     </div>
                     <div class="row">
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h1 class="font-light text-white font-20" id="_ganancia_hoy">0.08599345 <span>BTC</span></h1>

                        </div>
                        <div class="col-6 p-t-10 p-b-20 align-self-center">
                           <h1 class="font-light text-white font-20" id="_ganancia_ayer">0.08599384 <span>BTC</span>
                           </h1>

                        </div>
                        <br>
                        <br>
                     </div>
                  </div>
               </div>
            </div>


            <div class="col-lg-6">
               <div class="card card-inverse text-default">

                  <div class="card-body text-center  text-default">
                     <div class="d-flex row">
                        <div class="col-md-12 m-r-20">
                           <h1 class=""><i class="fa  fa-dollar"></i></h1>
                        </div>
                        <div class="col-md-12">
                           <h3 class="card-title   text-primary">Ingresos de la Cuenta</h3>
                        </div>
                     </div>
                     <div class="row">
                        <h3 class="col-6  font-light text-left font-16">Balance</h3>
                        <h3 class="col-6  font-light text-right font-16" id="_balance">0.08686829 <span>BTC</span></h3>

                        <h3 class="col-6 font-light  font-16 text-left">Total Pagado</h3>
                        <h3 class="col-6 font-light  font-16 text-right" id="total_pagado">0.08686829 <span>BTC</span>
                        </h3>

                        <h3 class="col-6 font-light font-16 text-left">Ultimo pago</h3>
                        <h3 class="col-6 font-light  font-16 text-right" id="_ultimo_pago">0.086 <span>BTC</span></h3>

                        <h3 class="col-6 font-light  font-16 text-left">Pago pendiente</h3>
                        <h3 class="col-6 font-light text-default font-16 text-right" id="_pago_pendiente">0.0
                           <span>BTC</span></h3>
                     </div>

                  </div>
               </div>
            </div>
         </div>
         <!-- Column -->
      </div>

      <div class="col-lg-12">
         <!-- Column -->
         <div class="card">
            <div class="card-body">
               <!-- Row -->
               <div class="row">
                  <div class="col-12">
                     <h2>Pool Hashrate </i></h2>
                     <h2 id="_grafica" class="text-primary"></h2>
                  </div>
               </div>
            </div>
         </div>
         <!-- Column -->
      </div>
      <!-- Column -->
   </div>
@endsection

@section('scripts')
   <script src="{{ asset('assets/js/api_misa.js') }}"></script>

@endsection
