@extends('layouts/admin/default')
@section('page-titles')
<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Empresa</h4>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                <li class="breadcrumb-item active">Configuracion de Empresa</li>
            </ol>
        </div>
</div>
@stop
@section('content')
<!-- Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Configuracion de Empresa</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('company.store') }}" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <h3 class="box-title">Datos Basicos</h3>
                        <hr class="m-t-0 m-b-40">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Rif/Identificacion</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="rif" name="rif" placeholder="">
                                        <!--<small class="form-control-feedback"> This is inline help </small> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Nombre de la Empresa</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="">
                                        <!--<small class="form-control-feedback"> This is inline help </small> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Correo Electronico</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="">
                                        <!--<small class="form-control-feedback"> This is inline help </small> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <!--/span-->
                        <h3 class="box-title">Direccion</h3>
                        <hr class="m-t-0 m-b-40">
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Direccion</label>
                                    <div class="col-md-8">
                                        <input type="text" id="direccion" name="direccion" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Telefono</label>
                                    <div class="col-md-8">
                                        <input type="text" id="telefono" name="telefono" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <h3 class="box-title">Datos de Billetera</h3>
                        <hr class="m-t-0 m-b-40">
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Url</label>
                                    <div class="col-md-8">
                                        <input type="text" id="url" name="url" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Email</label>
                                    <div class="col-md-8">
                                        <input type="text" id="bill_email" name="bill_email" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Client_id</label>
                                    <div class="col-md-8">
                                        <input type="text" id="client_id" name="client_id" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Secret_client</label>
                                    <div class="col-md-8">
                                        <input type="text" id="secret_client" name="secret_client" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    </hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                        <button type="button" class="btn btn-inverse">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop