@extends('layouts/admin/default')

@section('page-titles')
<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Cargo</h4>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                <li class="breadcrumb-item active">Registro de Cargo</li>
            </ol>
        </div>
</div>
@stop

@section('content')
<!-- Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Nuevo Cargo</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('workplace.store') }}" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Nombre del Cargo</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="work_name" placeholder="">
                                        <!--<small class="form-control-feedback"> This is inline help </small> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Alias del Cargo</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="work_alias" placeholder="">
                                        <small class="form-control-feedback"> Nombre corto </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--/row-->
                        <div class="row">
                            
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Estatus</label>
                                    <div class="col-md-8">
                                        <div class="radio-list">
                                            <label class="custom-control custom-radio">
                                                <input id="activo" name="work_estatus" value="1" type="radio" checked="" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Activo</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input id="inactivo" name="work_estatus" value="2" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Inactivo</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
      
                        <!--/row-->
                    </div>
                    <hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                        <button type="button" class="btn btn-inverse">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop