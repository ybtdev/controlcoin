@extends('layouts/admin/default')

{{-- Page title --}}
@section('title')
    Bienvenido a la plataforma 
    @parent
@stop
@section('page-titles')
<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Cargo</h4>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                <li class="breadcrumb-item active">Lista de Cargos</li>
            </ol>
        </div>
</div>
@stop
@section('content')
<div class="row">
    <!-- Column -->
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a class="btn btn-success" href="{{ route('workplace.create') }}"><i class="fa fa-plus"></i> Nuevo Cargo</a>
                            </div>
                        </div>
                    </div>
                    </br>
                    <h4 class="card-title">Lista de Cargos</h4>
                    <div class="table-responsive m-t-40">
                        <table id="cargos" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Alias</th>
                                    <th>Estatus</th>
                                    <th>Operaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($workplace as $work)
                                    <tr>
                                        <td>{{ $work->work_id }}</td>
                                        <td>{{ $work->work_name }}</td>
                                        <td>{{ $work->work_alias }}</td>
                                        <td>{{ $work->work_estatus }}</td>
                                        
                                        <td>
                                            <a class="btn btn-success btn-xs" href="{{ route('workplace.show',['id' => $work->work_id] )}}" ><i class="fa fa-binoculars"></i></a> 
                                            <a class="btn btn-info btn-xs" href="{{ route('workplace.edit',['id' => $work->work_id] )}}" ><i class="fa fa-pencil"></i></a> 
                                            <a class="btn btn-danger btn-xs" href="{{ route('workplace.destroy',['id' => $work->work_id] )}}" ><i class="fa fa-trash-o"></i></a>
                                        </td>
                     
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
                    
    </div>
@stop
@section('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).ready(function() {
         console.log( "estoy en cargos!" );
         $('#cargos').DataTable( {
            "language": {
                    "decimal":        "",
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtrado de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primeo",
                        "last":       "Ultimo",
                        "next":       "Proximo",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": activar para ordenar la columna ascendente",
                        "sortDescending": ": activar para ordenar la columna descendente"
                    }
                }
        });
        
    });
</script>
@stop