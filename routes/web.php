<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
})->middleware('guest');

Route::post('/', 'UserController@login')->name('user.login');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/',  function () {
	    return view('layouts.index');
	});

	Route::get('mineria', function () {
        return view('layouts.mineros.index');
	});

    //dashboard
    Route::get('realtime', 'BTController@realTime')->name('btc.realtime');
    Route::get('mineros', 'BTController@mineros')->name('btc.mineros');
    Route::get('gananciasHoy', 'BTController@gananciasHoy')->name('btc.gananciasHoy');
    Route::get('hashRed', 'BTController@gananciasHoy')->name('btc.hashred');
    Route::get('poolHasrate', 'BTController@poolHasrate')->name('btc.poolHasrate');
    Route::get('usuario', 'BTController@usuario')->name('btc.usuario');
    Route::get('grafica', 'BTController@grafica')->name('btc.grafica');
    Route::get('networkStatus', 'BTController@networkStatus')->name('btc.networkStatus');
    Route::get('logout', 'UserController@logOut')->name('btc.logout');

    //tabla mienria, datos mineros
    Route::get('datosMineros', 'BTController@datosMineros')->name('btc.datosMineros');



    Route::get('/admin', function () {

        //return view('layouts.admin.index');
    });
});



